## Create a sonarqube project for each app in NX mono-repo

## Prerequisites

- Node.js 12 ( Node.js 10 would also work, but 12 is used when develop & test)
- Docker compose

## Validation

### Run local sonarqube

```
docker-compose up -d
```

Access http://localhost:9000 and login with `admin`

### Create sonar project

1. New project.![](images/new-project.jpg)
2. Set project key `set the projectKey of sonar.json`. ![](images/set-project-key.jpg)
3. Use existing token `set the token of sonar.json`. ![](images/use-existing-token.jpg)
4. Run analysis. ![](images/run-analysis.jpg)

> create every app to the sonarqube

### Analysis affected project

```
git init
git add README.md
git commit -m 'add readme'
git add .
git commit -m 'init'
npm i
npm run sonar
```

Access http://localhost:9000/projects to verify.

